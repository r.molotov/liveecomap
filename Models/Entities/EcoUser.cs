﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Models.Entities
{
    public class EcoUser : IdentityUser
    {
        public string NickName { get; set; }
        public int Rating { get; set; }

        public virtual ICollection<Marker> Markers { get; set; }
        public virtual List<MarkerUser> MarkerUsers { get; set; }
    }
}
