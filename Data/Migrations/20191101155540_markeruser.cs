﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LiveEcoMap.Data.Migrations
{
    public partial class markeruser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "NickName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Rating",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MarkerCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    IconPath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarkerCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MarkerStates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<int>(nullable: false),
                    Color = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarkerStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Markers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(nullable: true),
                    PointLatitude = table.Column<float>(nullable: false),
                    PointLongitude = table.Column<float>(nullable: false),
                    MarkerCategoryId = table.Column<int>(nullable: false),
                    MarkerStateId = table.Column<int>(nullable: false),
                    OwnerId = table.Column<int>(nullable: false),
                    OwnerId1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Markers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Markers_MarkerCategories_MarkerCategoryId",
                        column: x => x.MarkerCategoryId,
                        principalTable: "MarkerCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Markers_MarkerStates_MarkerStateId",
                        column: x => x.MarkerStateId,
                        principalTable: "MarkerStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Markers_AspNetUsers_OwnerId1",
                        column: x => x.OwnerId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MarkerUsers",
                columns: table => new
                {
                    MarkerId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarkerUsers", x => new { x.MarkerId, x.UserId });
                    table.ForeignKey(
                        name: "FK_MarkerUsers_Markers_MarkerId",
                        column: x => x.MarkerId,
                        principalTable: "Markers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MarkerUsers_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Markers_MarkerCategoryId",
                table: "Markers",
                column: "MarkerCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Markers_MarkerStateId",
                table: "Markers",
                column: "MarkerStateId");

            migrationBuilder.CreateIndex(
                name: "IX_Markers_OwnerId1",
                table: "Markers",
                column: "OwnerId1");

            migrationBuilder.CreateIndex(
                name: "IX_MarkerUsers_UserId",
                table: "MarkerUsers",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MarkerUsers");

            migrationBuilder.DropTable(
                name: "Markers");

            migrationBuilder.DropTable(
                name: "MarkerCategories");

            migrationBuilder.DropTable(
                name: "MarkerStates");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "NickName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Rating",
                table: "AspNetUsers");
        }
    }
}
