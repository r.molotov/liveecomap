﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LiveEcoMap.Data.Migrations
{
    public partial class ownerid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Markers_AspNetUsers_OwnerId1",
                table: "Markers");

            migrationBuilder.DropIndex(
                name: "IX_Markers_OwnerId1",
                table: "Markers");

            migrationBuilder.DropColumn(
                name: "OwnerId1",
                table: "Markers");

            migrationBuilder.AlterColumn<string>(
                name: "OwnerId",
                table: "Markers",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Markers_OwnerId",
                table: "Markers",
                column: "OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Markers_AspNetUsers_OwnerId",
                table: "Markers",
                column: "OwnerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Markers_AspNetUsers_OwnerId",
                table: "Markers");

            migrationBuilder.DropIndex(
                name: "IX_Markers_OwnerId",
                table: "Markers");

            migrationBuilder.AlterColumn<int>(
                name: "OwnerId",
                table: "Markers",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerId1",
                table: "Markers",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Markers_OwnerId1",
                table: "Markers",
                column: "OwnerId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Markers_AspNetUsers_OwnerId1",
                table: "Markers",
                column: "OwnerId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
