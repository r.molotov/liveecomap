﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LiveEcoMap.Models;
using LiveEcoMap.Data;

namespace LiveEcoMap.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        public IActionResult Account()
        {
            return View(ApplicationManager.GetInstance().dbContext.EcoUsers.First());
        }
        public IActionResult Test()
        {
            return View(ApplicationManager.GetInstance().dbContext.MarkerStates.ToList());
        }

        public IActionResult Map()
        {
            return View(ApplicationManager.GetInstance().dbContext.Markers.ToList());
        }
        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
